require ["fileinto"];
# rule:[SPAM]
if header :contains "X-Spam-Flag" "YES"
{
	fileinto "Spam";
	stop;
}
# rule:[SPAM2]
if header :matches "Subject" ["*money*","*Viagra*","Cialis"]
{
	fileinto "Spam";
}
# rule:[LogWatch]
if header :contains "subject" "Logwatch"
{
	fileinto "INBOX.LogWatch";
}
# rule:[Cron]
if header :contains "subject" "Cron"
{
	fileinto "INBOX.Cron";
}
# rule:[Netwatch]
if header :contains "subject" "Vlad"
{
	fileinto "INBOX.Netwatch";
}
